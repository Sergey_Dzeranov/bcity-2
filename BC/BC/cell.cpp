#include "cell.h"

Cell::Cell( QOpenGLShaderProgram *program,
            int vertexAttr, int textureAttr,
            int textureUniform ) :
    timeFromUpdate ( 0 )
{
    sprite = new Sprite ( program, vertexAttr, textureAttr, textureUniform );
    spriteAnimation = new SpriteAnimation ( program, vertexAttr, textureAttr, textureUniform );
}

Cell::~Cell()
{

}


void Cell::draw()
{
    if (isAnimated)
        spriteAnimation->draw();
    else
        sprite->draw();
}

void Cell::update()
{
    timeFromUpdate += 25;
    if ( timeFromUpdate >= ( 1000 / spriteAnimation->fps ))
    {
        draw();
        timeFromUpdate = 0;
    }
}

