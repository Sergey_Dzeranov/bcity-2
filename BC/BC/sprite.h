#ifndef SPRITE_H
#define SPRITE_H


#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <vector>
#include <QRect>

class Sprite
{
public:
    Sprite( QOpenGLShaderProgram *program, int vertexAttr,
           int textureAttr, int textureUniform );
    ~Sprite();

    void draw();
    void setTexture( QOpenGLTexture *texture );

public:
    QRect *rect;


    void initVertices();
    void initTextureCoords();
    std::vector<float> m_vertices;
    std::vector<float> m_textureCoords;
    QOpenGLTexture *m_texture;
    QOpenGLShaderProgram *m_program;
    int m_vertexAttr;
    int m_textureAttr;
    int m_textureUniform;
};

#endif // SPRITE_H
