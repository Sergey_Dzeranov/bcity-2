#ifndef CELL_H
#define CELL_H

#include <QOpenGLShaderProgram>
#include "sprite.h"
#include "spriteanimation.h"
#include "gameobject.h"

class Cell : public GameObject
{
public:
    Cell( QOpenGLShaderProgram *program, int vertexAttr,
          int textureAttr, int textureUniform );
    ~Cell();

    void update() override;
    void draw() override;
public:
    Sprite *sprite;
    SpriteAnimation *spriteAnimation;
    QOpenGLShaderProgram *m_program;
    int m_vertexAttr;
    int m_textureAttr;
    int m_textureUniform;
    int counter;
    int timeFromUpdate;
    bool isAnimated;

};

#endif // CELL_H
