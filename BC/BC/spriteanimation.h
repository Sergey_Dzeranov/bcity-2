#ifndef SPRITEANIMATION_H
#define SPRITEANIMATION_H


#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <vector>
#include <QRect>

class SpriteAnimation
{
public:
    SpriteAnimation( QOpenGLShaderProgram *program, int vertexAttr,
           int textureAttr, int textureUniform );
    ~SpriteAnimation();
    void draw();
    void setTexture( QOpenGLTexture *texture );
    void initVertices();
    void initTextureCoords();

public:
    QRect *rect;
    std::vector<float> m_vertices;
    std::vector<float> m_textureCoords;
    std::vector<QOpenGLTexture*> m_textures;
    QOpenGLTexture *m_texture;
    QOpenGLShaderProgram *m_program;
    int m_vertexAttr;
    int m_textureAttr;
    int m_textureUniform;
    int fps;
    int counter;
};
#endif // SPRITEANIMATION_H
