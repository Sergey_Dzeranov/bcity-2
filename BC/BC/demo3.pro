#-------------------------------------------------
#
# Project created by QtCreator 2015-07-27T13:58:04
#
#-------------------------------------------------

QT       += core gui opengl

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = demo3
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp \
    scene.cpp \
    eagle.cpp \
    tank.cpp \
    sprite.cpp \
    cell.cpp \
    gameobject.cpp \
    spriteanimation.cpp

HEADERS  += dialog.h \
    scene.h \
    eagle.h \
    tank.h \
    sprite.h \
    cell.h \
    gameobject.h \
    spriteanimation.h

FORMS    += dialog.ui

RESOURCES += \
    shaders.qrc \
    textures.qrc \
    map.qrc
