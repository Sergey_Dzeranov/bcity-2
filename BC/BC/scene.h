#ifndef SCENE_H
#define SCENE_H

#include <QString>
#include <QFile>
#include <QOpenGLWidget>
#include <QOpenGLShaderProgram>
#include <vector>
#include <map>
#include <QKeyEvent>
#include <QTimer>
#include "eagle.h"
#include "tank.h"
#include "cell.h"

class Scene : public QOpenGLWidget
{
    Q_OBJECT
public:
    Scene( QWidget *parent = 0 );
    ~Scene();

public slots:
    void gameLoop();

private:
    void initializeGL();
    void paintGL();
    void resizeGL( int w, int h );
    void keyPressEvent( QKeyEvent *event );
    void readFileMap();
    void generateTextures();
    QOpenGLShaderProgram m_program;
    int m_vertexAttr;
    int m_textureAttr;
    int m_textureUniform;
    int m_matrixUniform;
    Eagle *m_eagle;
    Tank *m_tank;
    float m_scale;

    std::vector<Cell*> m_cell;
    int mapMatrix[29][29];
    int xIndex, yIndex;
};

#endif // SCENE_H
