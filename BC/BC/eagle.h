#ifndef EAGLE_H
#define EAGLE_H

#include "sprite.h"

class Eagle
{
public:
    Eagle( QOpenGLShaderProgram *program, int vertexAttr, int textureAttr, int textureUniform );
    ~Eagle();
    void setHealth( int hp );
    int health() const;

public:
    Sprite *sprite;

private:
    QOpenGLTexture *m_texture;
    int m_health;
};

#endif // EAGLE_H

