#include "tank.h"
#include <QDebug>

Tank::Tank( QOpenGLShaderProgram *program,
            int vertexAttr, int textureAttr,
            int textureUniform)
{
    sprite = new Sprite ( program, vertexAttr, textureAttr, textureUniform );
    genTextures();
    setDirection( Tank::Up );
}

Tank::~Tank()
{
    delete m_upTexture;
    delete m_downTexture;
    delete m_rightTexture;
    delete m_leftTexture;
}

void Tank::genTextures()
{
    QImage image( ":/Textures/TankSpriteSheet.png" );

    std::vector<QImage> frames;
    QImage frame = image.copy( 0, 0, 16, 16 );
    frame = frame.mirrored( false, true );
    frames.push_back(frame);

    frame = image.copy( 32, 0, 16, 16 );
    frame = frame.mirrored( false, true );
    frames.push_back(frame);

    frame = image.copy( 64, 0, 16, 16 );
    frame = frame.mirrored( false, true );
    frames.push_back(frame);

    frame = image.copy( 96, 0, 16, 16 );
    frame = frame.mirrored( false, true );
    frames.push_back(frame);


    m_upTexture = new QOpenGLTexture(frames[0]);
    m_leftTexture = new QOpenGLTexture(frames[1]);
    m_downTexture = new QOpenGLTexture(frames[2]);
    m_rightTexture = new QOpenGLTexture(frames[3]);
}
Tank::Direction Tank::direction() const
{
    return m_direction;
}

void Tank::setDirection( Tank::Direction dir )
{
    m_direction = dir;

    switch( m_direction )
    {
        case Tank::Up:
            sprite->setTexture( m_upTexture );
            break;
        case Tank::Left:
            sprite->setTexture( m_leftTexture);
            break;
        case Tank::Down:
            sprite->setTexture( m_downTexture );
            break;
        case Tank::Right:
            sprite->setTexture( m_rightTexture );
            break;
    }
}

void Tank::nextFrame()
{
    switch( m_direction )
    {
        case Tank::Up:
            sprite->setTexture( m_upTexture );
            break;
        case Tank::Left:
            sprite->setTexture( m_leftTexture );
            break;
        case Tank::Down:
            sprite->setTexture( m_downTexture );
            break;
        case Tank::Right:
            sprite->setTexture( m_rightTexture );
            break;
    }
}
