#include "eagle.h"

Eagle::Eagle( QOpenGLShaderProgram *program,
              int vertexAttr, int textureAttr,
              int textureUniform ) :
    m_health (1)
{
    sprite = new Sprite ( program, vertexAttr, textureAttr, textureUniform );
    QImage image( ":/Textures/TankSpriteSheet.png" );
    QImage frame = image.copy( 304, 32, 16, 16 );
    frame = frame.mirrored( false, true );
    m_texture = new QOpenGLTexture( frame );
    sprite->setTexture( m_texture );
}

Eagle::~Eagle()
{
    delete m_texture;
}

void Eagle::setHealth(int hp )
{
    m_health = hp;
}

int Eagle::health() const
{
    return m_health;
}

