#ifndef TANK_H
#define TANK_H

#include "sprite.h"
#include <QObject>
#include <vector>

class Tank : public QObject
{
    Q_OBJECT
public:
    Tank( QOpenGLShaderProgram *program,
          int vertexAttr,
          int textureAttr,
          int textureUniform );
    ~Tank();
    enum Direction { Up, Down, Left, Right };
    Direction direction() const;
    void setDirection( Direction dir );
    void nextFrame();

public:
    Sprite *sprite;

private:
    void genTextures();

    QOpenGLTexture* m_upTexture;
    QOpenGLTexture* m_downTexture;
    QOpenGLTexture* m_rightTexture;
    QOpenGLTexture* m_leftTexture;

    Direction m_direction;
};

#endif // TANK_H

