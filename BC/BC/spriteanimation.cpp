#include "spriteanimation.h"

SpriteAnimation::SpriteAnimation( QOpenGLShaderProgram *program, int vertexAttr,
              int textureAttr, int textureUniform ) :
    m_program( program ),
    m_vertexAttr( vertexAttr ),
    m_textureAttr( textureAttr ),
    m_textureUniform( textureUniform ),
    counter ( 0 ),
    fps ( 4 )
{
    rect = new QRect();
    initTextureCoords();
}

SpriteAnimation::~SpriteAnimation()
{

}

void SpriteAnimation::draw()
{
    initVertices();
    m_textures[counter]->bind();
    m_program->setAttributeArray( m_vertexAttr, m_vertices.data(), 3 );
    m_program->setAttributeArray( m_textureAttr, m_textureCoords.data(), 2 );
    m_program->setUniformValue( m_textureUniform, 0 );

    m_program->enableAttributeArray( m_vertexAttr );
    m_program->enableAttributeArray( m_textureAttr );

    glDrawArrays( GL_TRIANGLES, 0, 6 );
    m_program->disableAttributeArray( m_vertexAttr );
    m_program->disableAttributeArray( m_textureAttr );
    counter++;
    if (counter >= m_textures.size() )
        counter = 0;
}

void SpriteAnimation::initVertices()
{
    m_vertices.resize( 18 );

    // 0
    m_vertices[0] = rect->x();
    m_vertices[1] = rect->y();
    m_vertices[2] = 0.0f;

    // 1
    m_vertices[3] = rect->x();
    m_vertices[4] = rect->y() + rect->height();
    m_vertices[5] = 0.0f;

    // 2
    m_vertices[6] = rect->x() + rect->width();
    m_vertices[7] = rect->y();
    m_vertices[8] = 0.0f;

    // 3
    m_vertices[9] = rect->x() + rect->width();
    m_vertices[10] = rect->y();
    m_vertices[11] = 0.0f;

    // 4
    m_vertices[12] = rect->x();
    m_vertices[13] = rect->y() + rect->height();
    m_vertices[14] = 0.0f;

    // 5
    m_vertices[15] = rect->x() + rect->width();
    m_vertices[16] = rect->y() + rect->height();
    m_vertices[17] = 0.0f;
}

void SpriteAnimation::initTextureCoords()
{
    m_textureCoords.resize( 12 );

    // 0
    m_textureCoords[0] = 0.0f;
    m_textureCoords[1] = 1.0f;

    // 1
    m_textureCoords[2] = 0.0f;
    m_textureCoords[3] = 0.0f;

    // 2
    m_textureCoords[4] = 1.0f;
    m_textureCoords[5] = 1.0f;

    // 3
    m_textureCoords[6] = 1.0f;
    m_textureCoords[7] = 1.0f;

    // 4
    m_textureCoords[8] = 0.0f;
    m_textureCoords[9] = 0.0f;

    // 5
    m_textureCoords[10] = 1.0f;
    m_textureCoords[11] = 0.0f;
}

void SpriteAnimation::setTexture( QOpenGLTexture *texture )
{
    m_texture = texture;
}
