#include <QMatrix4x4>
#include <QDebug>
#include "scene.h"

Scene::Scene( QWidget *parent ) :
    QOpenGLWidget( parent )
{
   setFocusPolicy( Qt::StrongFocus );
   readFileMap();
   QTimer *timer = new QTimer(this);
   connect(timer, SIGNAL(timeout()), this, SLOT(gameLoop()));
   timer->start(25);
}

Scene::~Scene()
{
    makeCurrent();
    delete m_eagle;
    delete m_tank;
    for ( size_t i = 0; i < m_cell.size(); ++i )
        delete m_cell[i];
    doneCurrent();
}

void Scene::initializeGL()
{
    glClearColor( 0.1f, 0.1f, 0.2f, 1.0f );

    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glEnable( GL_BLEND );

    QOpenGLShader vShader( QOpenGLShader::Vertex );
    vShader.compileSourceFile( ":/Shaders/vShader.glsl" );

    QOpenGLShader fShader( QOpenGLShader::Fragment );
    fShader.compileSourceFile( ":/Shaders/fShader.glsl" );

    m_program.addShader( &vShader );
    m_program.addShader( &fShader );
    if ( !m_program.link() )
    {
        qWarning( "Error: unable to link a shader program." );
        return;
    }
    m_vertexAttr = m_program.attributeLocation( "vertexAttr" );
    m_textureAttr = m_program.attributeLocation( "textureAttr" );
    m_textureUniform = m_program.uniformLocation( "textureUniform" );
    m_matrixUniform = m_program.uniformLocation( "matrix" );
    generateTextures();
    m_eagle = new Eagle( &m_program, m_vertexAttr, m_textureAttr, m_textureUniform );
    m_eagle->sprite->rect->setRect(1400 / 2, 1400 - 50, 50, 50);
    m_tank = new Tank ( &m_program, m_vertexAttr, m_textureAttr, m_textureUniform );
    m_tank->sprite->rect->setRect( m_eagle->sprite->rect->x() - 100.0f,m_eagle->sprite->rect->y() - 10.0f, 50, 50 );
}

void Scene::paintGL()
{
    glClear( GL_COLOR_BUFFER_BIT );

    if ( !m_program.bind() )
        return;

    QMatrix4x4 matrix;
    matrix.ortho( 0.0f, 725.0f, 725.0f, 0.0f, -2.0f, 2.0f );
    matrix.scale( 0.5f );
    m_program.setUniformValue( m_matrixUniform, matrix );
    m_tank->sprite->draw();
    for ( size_t i = 0; i < m_cell.size(); ++i )
            m_cell[i]->draw(); //А здесь вызывается draw,и в итоге у меня происходит перерисовка анимации каждый раз,когда вызывается force redraw
    m_eagle->sprite->draw();

    m_program.release();
    qDebug()<<"PAINTGL";
}

void Scene::resizeGL( int w, int h )
{
    glViewport( 0, 0, w, h );
}


void Scene::keyPressEvent( QKeyEvent *event )
{
    const float step = 10.0f;

    switch( event->key() )
    {
        case Qt::Key_Up:
            xIndex = (m_tank->sprite->rect->x() + m_tank->sprite->rect->width() - step) / 50;
            yIndex = (m_tank->sprite->rect->y() - step) / 50;
            if (mapMatrix[yIndex][xIndex-1] == 0 && mapMatrix[yIndex][xIndex] == 0 )
                m_tank->sprite->rect->setRect(m_tank->sprite->rect->x(), m_tank->sprite->rect->y() - step , 50 ,50);
            m_tank->setDirection( Tank::Up );
            m_tank->nextFrame();
            break;
        case Qt::Key_Left:
            xIndex = (m_tank->sprite->rect->x() - 2*step) / 50;
            yIndex = m_tank->sprite->rect->y() / 50;
            if (mapMatrix[yIndex][xIndex] == 0 && mapMatrix[yIndex+1][xIndex] == 0)
                m_tank->sprite->rect->setRect( m_tank->sprite->rect->x() - step,m_tank->sprite->rect->y() , 50, 50 );
            m_tank->setDirection( Tank::Left );
            m_tank->nextFrame();
            break;
        case Qt::Key_Right:
            xIndex = (m_tank->sprite->rect->x() + m_tank->sprite->rect->width() + step) / 50;
            yIndex = m_tank->sprite->rect->y() / 50;
            if (mapMatrix[yIndex][xIndex] == 0 && mapMatrix[yIndex+1][xIndex] == 0)
                m_tank->sprite->rect->setRect( m_tank->sprite->rect->x() + step, m_tank->sprite->rect->y(), 50, 50 );
            m_tank->setDirection( Tank::Right );
            m_tank->nextFrame();
            break;
        case Qt::Key_Down:
        xIndex = (m_tank->sprite->rect->x() + m_tank->sprite->rect->width() - step) / 50;
        yIndex = (m_tank->sprite->rect->y() + m_tank->sprite->rect->height() + step) / 50;
        if (mapMatrix[yIndex][xIndex-1] == 0 && mapMatrix[yIndex][xIndex] == 0 )
                m_tank->sprite->rect->setRect(m_tank->sprite->rect->x(), m_tank->sprite->rect->y() + step, 50 ,50 );
            m_tank->setDirection( Tank::Down );
            m_tank->nextFrame();
            break;
    }
    //update();
}

void Scene::readFileMap()
{
    QFile file("D:\\QtProj\\demo3\\Map\\map.txt"); //Не хочет читать txt файл из ресурса. (????)
    file.open(QIODevice::ReadWrite | QIODevice::Text);
    QTextStream in(&file);
    QString string;
    int i = 0;
    while (!in.atEnd())
    {
        if (i == 29)
            break;
        string = in.readLine();
        for (int j = 0; j < string.size(); j++)
            mapMatrix[i][j] = string[j].digitValue();
        i++;
    }
}

void Scene::generateTextures()
{
    Cell *cell;
    QImage image( ":/Textures/TankSpriteSheet.png" );
    QImage frame;
    for (int i = 0; i < 29; i++)
        for (int j = 0; j < 29; j++)
        {
            int texture = mapMatrix[i][j];
            switch(texture)
            {
            case 0:
                break;
            case 1:
                cell = new Cell ( &m_program, m_vertexAttr, m_textureAttr, m_textureAttr ); // IRON
                cell->sprite->rect->setRect(j * 50, i * 50, 50, 50);
                frame = image.copy( 256, 16, 16, 16 );
                frame = frame.mirrored( false, true );
                cell->sprite->m_texture = new QOpenGLTexture( frame );
                cell->sprite->setTexture( cell->sprite->m_texture );
                cell->isAnimated = false;
                m_cell.push_back( cell );
                break;
            case 2:
                cell = new Cell ( &m_program, m_vertexAttr, m_textureAttr, m_textureAttr ); // BRICK
                cell->sprite->rect->setRect(j * 50, i * 50, 50, 50);
                frame = image.copy( 256, 0, 16, 16 );
                frame = frame.mirrored( false, true );
                cell->sprite->m_texture = new QOpenGLTexture( frame );
                cell->sprite->setTexture( cell->sprite->m_texture );
                cell->isAnimated = false;
                m_cell.push_back( cell );
                break;
            case 4:
                cell = new Cell ( &m_program, m_vertexAttr, m_textureAttr, m_textureAttr ); // BORDER
                cell->sprite->rect->setRect(j * 50, i * 50, 50, 50);
                frame = image.copy( 370, 0, 16, 16 );
                frame = frame.mirrored( false, true );
                cell->sprite->m_texture = new QOpenGLTexture( frame );
                cell->sprite->setTexture( cell->sprite->m_texture );
                cell->isAnimated = false;
                m_cell.push_back( cell );
                break;
            case 7:
                std::vector<QImage> frames;
                cell = new Cell ( &m_program, m_vertexAttr, m_textureAttr, m_textureAttr ); //WATER
                cell->spriteAnimation->rect->setRect(j * 50, i * 50, 50, 50);
                frame = image.copy( 256, 48, 16, 16 );
                frame = frame.mirrored( false, true );
                frames.push_back( frame );
                frame = image.copy( 272, 48, 16, 16 );
                frame = frame.mirrored( false, true );
                frames.push_back( frame );
                frame = image.copy( 256, 32, 16, 16 );
                frame = frame.mirrored( false, true );
                frames.push_back( frame );
                cell->spriteAnimation->m_textures.push_back( new QOpenGLTexture (frames[0]) );
                cell->spriteAnimation->m_textures.push_back( new QOpenGLTexture (frames[1]) );
                cell->spriteAnimation->m_textures.push_back( new QOpenGLTexture (frames[2]) );
                cell->spriteAnimation->setTexture( cell->spriteAnimation->m_textures[0] );
                cell->isAnimated = true;
                m_cell.push_back( cell );
                break;
            }
        }
}

void Scene::gameLoop()
{
    for ( size_t i = 0; i < m_cell.size(); ++i)
        m_cell[i]->update();

    // force qt widget redraw
    update(); //Здесь,собственно,вызывается paintGL
}
